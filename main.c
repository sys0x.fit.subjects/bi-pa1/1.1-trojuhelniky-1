#include <stdio.h>
#include <math.h>
#include <stdlib.h>

#define DBL_EPSILON 2.2204460492503131e-16 // double
int invalidInput(void){
    printf("Nespravny vstup.\n");
    //return 1;
    exit(1);
}

int getInput(double * bodX ,double * bodY){
    if(scanf("%lf %lf", bodX, bodY)!=2){//&* bodX a &* bodY =====>&* se navzajem sezerou protoze to jsou inverzni funkce k sobe
        return invalidInput();
    }
    return 0;
}
int checkLine(double bodXA,double bodXB,double bodXC,double bodYA,double bodYB,double bodYC){
    double sideAB,sideAC,sideBC;//nejak to potom nepocitat 2x!!!!!!!!!!!!!!!!!!!!!!!
    double alfa, beta, gama;

    sideAB=sqrt(pow(bodXA-bodXB,2)+pow(bodYA-bodYB,2));
    sideAC=sqrt(pow(bodXA-bodXC,2)+pow(bodYA-bodYC,2));
    sideBC=sqrt(pow(bodXB-bodXC,2)+pow(bodYB-bodYC,2));
    alfa=(pow(sideBC, 2)-pow(sideAC, 2)-pow(sideAC, 2))/(-2*sideAC*sideAB);
    beta=(pow(sideAC, 2)-pow(sideAC, 2)-pow(sideBC, 2))/(-2*sideAB*sideBC);
    gama=(pow(sideAB, 2)-pow(sideAC, 2)-pow(sideBC, 2))/(-2*sideBC*sideAC);


    if(sideAB==0||sideAC==0||sideBC==0||fabs(fabs(alfa)-1)<=1000*DBL_EPSILON||fabs(fabs(beta)-1)<=1000*DBL_EPSILON||fabs(fabs(gama)-1)<=1000*DBL_EPSILON){
        printf("Body netvori trojuhelnik.\n");
        return 1;

    }



    return 0;





    /*int b=0;
    do{
    if(bodXA==0||bodXB==0||bodXC==0||bodYA==0||bodYB==0||bodYC==0){
        bodXA++;
        bodXB++;
        bodXC++;
        bodYA++;
        bodYB++;
        bodYC++;
    }else {
        b=1;
    }
    }while(b==0);

    double vektorX,vektorY,vektorZkouskaX,vektorZkouskaY, vektorZkouskaZkouskaY, vektorZkouskaZkouskaX;
    double a;
    vektorX=bodXB-bodXA;
    vektorY=bodYB-bodYA;

    vektorZkouskaX=bodXB-bodXC;
    vektorZkouskaY=bodYB-bodYC;

    vektorZkouskaZkouskaX=bodXA-bodXC;
    vektorZkouskaZkouskaY=bodYA-bodYC;
    a=vektorX/vektorZkouskaX;
    if(fabs(a*vektorZkouskaY-vektorY)<=1000*DBL_EPSILON*fabs(a*vektorZkouskaY+vektorY)||fabs(vektorX)+fabs(vektorZkouskaX)==0||fabs(vektorY)+fabs(vektorZkouskaY)==0||){

        printf("Body netvori trojuhelnik.\n");
        return 1;

    }
    return 0;*/
}
int sortEdges(double * sideAB,double * sideAC,double * sideBC){
    if(* sideAB<* sideAC){
        double temp= * sideAC;
        * sideAC=* sideAB;
        * sideAB=temp;
    }
    if(* sideAB<* sideBC){
        double temp= * sideBC;
        * sideBC=* sideAB;
        * sideAB=temp;
    }
    if(* sideAC<* sideBC){
        double temp= * sideBC;
        * sideBC=* sideAC;
        * sideAC=temp;
    }
    return 0;
}
int createEdges(double * bodXA,double * bodXB,double * bodXC,double * bodYA,double * bodYB,double * bodYC,double * sideAB,double * sideAC,double * sideBC ){
    * sideAB=sqrt(pow(* bodXA-* bodXB,2)+pow(* bodYA-* bodYB,2));
    * sideAC=sqrt(pow(* bodXA-* bodXC,2)+pow(* bodYA-* bodYC,2));
    * sideBC=sqrt(pow(* bodXB-* bodXC,2)+pow(* bodYB-* bodYC,2));
    return 0;
}

int main(void) {
    double bodXA1,bodXB1,bodXC1;
    double bodYA1,bodYB1,bodYC1;

    double bodXA2,bodXB2,bodXC2;
    double bodYA2,bodYB2,bodYC2;

    double sideAB1,sideAC1,sideBC1,sideAB2,sideAC2,sideBC2;

    printf("Trojuhelnik #1:\n");
    printf("Bod A:\n");
    getInput(&bodXA1,&bodYA1);
    /*
    if(scanf("%lf %lf",&bodXA1, &bodYA1)!=2){
        return invalidInput();
    }*/
    printf("Bod B:\n");
    getInput(&bodXB1,&bodYB1);
    /* if(scanf("%lf %lf",&bodXB1, &bodYB1)!=2){
         return invalidInput();
 }*/
    printf("Bod C:\n");
    getInput(&bodXC1,&bodYC1);
    /*if( scanf("%lf %lf",&bodXC1, &bodYC1)!=2){
        return invalidInput();
}*/
    if(checkLine(bodXA1,bodXB1,bodXC1,bodYA1,bodYB1,bodYC1)==1){
        return 1;
    }

    printf("Trojuhelnik #2:\n");
    printf("Bod A:\n");
    getInput(&bodXA2,&bodYA2);
    /*  if(scanf("%lf %lf",&bodXA2, &bodYA2)!=2){
          return invalidInput();
  }*/
    printf("Bod B:\n");
    getInput(&bodXB2,&bodYB2);
    /* if( scanf("%lf %lf",&bodXB2, &bodYB2)!=2){
         return invalidInput();
 }*/
    printf("Bod C:\n");
    getInput(&bodXC2,&bodYC2);
    /* if( scanf("%lf %lf",&bodXC2, &bodYC2)!=2){
         return invalidInput();
 }*/
    if(checkLine(bodXA2,bodXB2,bodXC2,bodYA2,bodYB2,bodYC2)==1){
        return 1;
    }

    //prevod na delky stran
    createEdges(&bodXA1,&bodXB1,&bodXC1,&bodYA1,&bodYB1,&bodYC1,&sideAB1,&sideAC1,&sideBC1);
    createEdges(&bodXA2,&bodXB2,&bodXC2,&bodYA2,&bodYB2,&bodYC2,&sideAB2,&sideAC2,&sideBC2);
    /*sideAB1=sqrt(pow(bodXA1-bodXB1,2)+pow(bodYA1-bodYB1,2));
    sideAC1=sqrt(pow(bodXA1-bodXC1,2)+pow(bodYA1-bodYC1,2));
    sideBC1=sqrt(pow(bodXB1-bodXC1,2)+pow(bodYB1-bodYC1,2));

    sideAB2=sqrt(pow(bodXA2-bodXB2,2)+pow(bodYA2-bodYB2,2));
    sideAC2=sqrt(pow(bodXA2-bodXC2,2)+pow(bodYA2-bodYC2,2));
    sideBC2=sqrt(pow(bodXB2-bodXC2,2)+pow(bodYB2-bodYC2,2));
*/
    //sort th{A;B;C}1
    sortEdges(&sideAB1,&sideAC1,&sideBC1);
    sortEdges(&sideAB2,&sideAC2,&sideBC2);
    /* if(sideAB1<sideAC1){
         double temp= sideAC1;
         sideAC1=sideAB1;
         sideAB1=temp;
     }
     if(sideAB1<sideBC1){
         double temp= sideBC1;
         sideBC1=sideAB1;
         sideAB1=temp;
     }
     if(sideAC1<sideBC1){
         double temp= sideBC1;
         sideBC1=sideAC1;
         sideAC1=temp;
     }*/

    //sort th{A;B;C}2
/*
    if (sideAB2 < sideAC2) {
        double temp = sideAC2;
        sideAC2 = sideAB2;
        sideAB2 = temp;
    }
    if (sideAB2 < sideBC2) {
        double temp = sideBC2;
        sideBC2 = sideAB2;
        sideAB2 = temp;
    }
    if (sideAC2 < sideBC2) {
        double temp = sideBC2;
        sideBC2 = sideAC2;
        sideAC2 = temp;
    }*/

    /* double isZero=fabs(sideAB1-sideAB2)+fabs(sideAC1-sideAC2)+fabs(sideBC1-sideBC2);
     double isCheckZero=(fabs(sideAB1+sideAB2)+fabs(sideAC1+sideAC2)+fabs(sideBC1+sideBC2));*/


    if((fabs(sideAB1-sideAB2)+fabs(sideAC1-sideAC2)+fabs(sideBC1-sideBC2))<=DBL_EPSILON*10000*(fabs(sideAB1+sideAB2)+fabs(sideAC1+sideAC2)+fabs(sideBC1+sideBC2))){
        printf("Trojuhelniky jsou shodne.\n");
        return 0;
    }

    double obvod1=(sideAB1+sideAC1+sideBC1);
    double obvod2=(sideAB2+sideAC2+sideBC2);
    /* double isObvodZero=(fabs((sideAB1+sideAC1+sideBC1)-(sideAB2+sideAC2+sideBC2)));
     double isCheckObvodZero=(fabs((sideAB1+sideAC1+sideBC1)+(sideAB2+sideAC2+sideBC2)));*/

    if(fabs(obvod1-obvod2)<=DBL_EPSILON*10000*fabs(obvod1+obvod2)){
        printf("Trojuhelniky nejsou shodne, ale maji stejny obvod.\n");

        return 0;
    }
    if(obvod1<obvod2){
        printf("Trojuhelnik #2 ma vetsi obvod.\n");

        return 0;
    }
    printf("Trojuhelnik #1 ma vetsi obvod.\n");

    return 0;




}